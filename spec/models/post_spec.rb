require 'rails_helper'

RSpec.describe Post, type: :model do
  
  before do 
    @post =  build(:post)
  end

  subject { @post }

  describe 'associations' do
    it { should belong_to(:user) }
  end 

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:description) }
  
  it "is valid" do
    expect(subject).to be_valid
  end

  it "is not valid without a title" do 
    subject.title = ''
    expect(subject).to_not be_valid
  end

  it "is not valid without a description" do 
    subject.description = ''
    expect(subject).to_not be_valid
  end

  it "is not valid without user" do 
    subject.user_id = nil
    expect(subject).to_not be_valid
  end
end