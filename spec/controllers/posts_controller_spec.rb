require 'rails_helper'

RSpec.describe PostsController do 
  

  describe 'GET #index by' do
    
    it 'unauthorized user' do
      expect(get :index).to redirect_to new_user_session_path
    end
    
    context 'authorized user' do 

      before do 
        sign_in create(:user, email: 'user@gmail.com')
      end

      it 'get index action' do  
        expect(get :index).to render_template("admin/posts/index")
      end

      it 'not redirect to sig in form' do
        expect(get :index).to_not redirect_to new_user_session_path
      end

    end

  end

end