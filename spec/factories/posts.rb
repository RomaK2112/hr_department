FactoryBot.define do
  
  factory :post do
    title { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    association :user, factory: :user
  end



end