FactoryBot.define do
  factory :user do
    first_name  { Faker::Name.first_name }
    last_name   { Faker::Name.last_name }
    email       { Faker::Internet.email }
    role    { 0 }
    password    { '12341234'}
    password_confirmation { '12341234' }

    after(:create) do |user|
      user.groups << FactoryBot.create(:group)
    end
  end

end