Rails.application.routes.draw do
  root to: 'home#index'
  
  resources :posts, only: [:index, :show]
  devise_for :users, controllers: { registrations: 'users/registrations' }

  namespace :admin do
    get '/', to: 'users#index'
    post 'groups/add_member'
    delete 'groups/destroy_member'

    resources :groups
    resources :posts
    resources :users
  end

end
