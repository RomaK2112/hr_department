class CreateJoinTableGroupsUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :groups_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :group, index: true
    end
  end
end
