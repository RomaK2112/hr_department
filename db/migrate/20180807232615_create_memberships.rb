class CreateMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :memberships do |t|
      t.integer :memberable_id
      t.string  :memberable_type
      t.integer :post_id
      t.timestamps
    end
  end
end
