# Create admin(HR) and user
User.create!({first_name: 'admin', last_name: 'admin', email: "admin@gmail.com", role: 1, password: "admin123", password_confirmation: "admin123" })
User.create!({first_name: 'user', last_name: 'user', email: "user@gmail.com", role: 0, password: "user123", password_confirmation: "user123" })

# Dummy users
20.times do 
  User.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email, role: 0, password: 'password', password_confirmation: 'password')
end

# Dummy posts
50.times do 
  Post.create!(title: Faker::Lorem.sentence, description: Faker::Lorem.paragraph, user_id: User.all.ids.sample)
end

#  Dummy groups
10.times do |i| 
  group = Group.create!(name: Faker::Lorem.sentence)
  group.users << User.where(id: User.all.ids.sample(3))
end

# Membership for users
10.times do |i|
   Membership.create!(memberable_id: User.all.ids.sample, memberable_type: 'User', post_id: Post.where.not(id: Post.joins(:membership).ids).ids.sample)
end

# Membership for groups
10.times do |i|
   Membership.create!(memberable_id: Group.all.ids.sample, memberable_type: 'Group', post_id: Post.where.not(id: Post.joins(:membership).ids).ids.sample)
end
