class PostMailer < ApplicationMailer

  def post_created(emails, post)
    @post = post
    mail(to: emails, subject: 'HR-department. New post shared for you!')
  end
end
