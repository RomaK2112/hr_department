class PostsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def index 
    @posts = Post.current_posts(current_user)
    @count = 0
    render 'admin/posts/index'
  end

  def show
    @post = Post.find(params[:id])
    render 'admin/posts/show'
  end
  
end
