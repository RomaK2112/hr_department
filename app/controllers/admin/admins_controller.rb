class Admin::AdminsController < ActionController::Base
  
  before_action :admin?

  layout 'admin'
  load_and_authorize_resource

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  private 

  def admin?
    redirect_to root_path, alert: 'You do not have permissions' unless current_user.admin?
  end

end