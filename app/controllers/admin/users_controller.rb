class Admin::UsersController < Admin::AdminsController

  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
    @count = 0
  end

  def show
  end

  def edit  
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to admin_user_path(@user)
    else
      render :edit
    end
  end

  def destroy
    if @user.delete
      flash[:notice] = 'Successfully deleted'
    else
      flash[:notice] = 'Not deleted'
    end
    redirect_to admin_users_path 
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name)
  end

end
