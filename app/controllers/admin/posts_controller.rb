  class Admin::PostsController < Admin::AdminsController

  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def new
    @post = Post.new
  end

  def index
    @count = 0 
    @posts = Post.all
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user

    if shared?
      @post.membership = Membership.new(memberable_type: params[:share_for], memberable_id: memberable_id) 
    end

    if @post.save!
      PostMailer.post_created(@post.recipients_emails, @post).deliver_now if shared?
      redirect_to admin_post_path(@post)
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @post.update_attributes(post_params)
      redirect_to admin_post_path(@post)
    else
      render :edit
    end
  end

  def destroy
    if @post.delete
      flash[:notice] = 'Successfully deleted'
    else
      flash[:notice] = 'Not deleted'
    end
    redirect_to admin_posts_path 
  end

  private

  def shared?
    params[:share_for] != 'None'
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :description, :file)
  end

  def memberable_id
    params[:share_for] == "User" ? params[:user] : params[:group]
  end

end
