class Admin::GroupsController < Admin::AdminsController
  
  before_action :set_group, only: [:show, :edit, :update, :destroy, :add_member, :destroy_member]
  
  def new
    @group = Group.new
  end

  def index
    @count  = 0
    @groups = Group.all
  end

  def show
    @count = 0
    @users = @group.users
  end

  def create
    @group = Group.new(group_params)
    if @group.save
      flash[:notice] = 'Successfully created'
    else
      render :new
    end
    redirect_to admin_group_path(@group.id)
  end

  def edit
  end

  def update
    if @group.update_attributes(group_params)
      flash[:notice] = 'Successfully updated'
    else
      render edit
    end
    redirect_to admin_group_path(@group.id)
  end

  def destroy
    if @group.destroy
      flash[:notice] = 'Successfully deleted'
    else
      flash[:notice] = 'Not deleted'
    end
    redirect_to admin__path 
  end

  def add_member
    @group.users << User.find(params[:user_id])
    redirect_to admin_group_path(@group.id)
  end

  def destroy_member
    @group.users.delete(User.find(params[:user_id]))
    flash[:notice] = 'Successfully deleted'
    redirect_to admin_group_path(@group.id) 
  end

  private

  def set_group
    @group = Group.find(params[:id])
  end

  def group_params
    params.require(:group).permit(:name)
  end

end
