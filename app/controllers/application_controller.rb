class ApplicationController < ActionController::Base 

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

  def authenticate_user!
    redirect_to new_user_session_path , alert: 'Not authorized.' if !signed_in?
  end

end
