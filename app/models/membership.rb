class Membership < ApplicationRecord
  belongs_to :memberable, polymorphic: true
  belongs_to :post
end
