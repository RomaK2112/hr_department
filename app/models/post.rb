class Post < ApplicationRecord
  
  belongs_to :user
  has_one :membership 
  has_one_attached :file 

  validates :description, :title, presence: true

  def recipients_emails
    if  self.membership.memberable_type == 'User'
      self.membership.memberable.email
    else
      self.membership.memberable.users.pluck(:email)
    end
  end

  def self.current_posts(user)
    mem_ids = (Membership.where(memberable_type: "User", memberable_id: user.id).ids << Membership.where(memberable_type: 'Group', memberable_id: user.groups.ids).ids).flatten
    Post.joins(:membership).where(memberships: {id: mem_ids})
  end

end
