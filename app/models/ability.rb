class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, :all
    else
      can :read, Post, id: Post.current_posts(user).ids
    end
  end

end
