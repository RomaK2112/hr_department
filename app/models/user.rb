class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :posts
  has_and_belongs_to_many :groups
  has_many :memberships, as: :memberable

  enum role: [:user, :admin]

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def admin?
    self.role == 'admin'
  end

  def self.not_group_members(group_id)
    User.where.not(id: Group.find(group_id).users.ids)
  end

end
